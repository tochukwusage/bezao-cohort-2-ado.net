﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BEZAOPayDAL.Models;

// system connection, system command, and system dataReader.

namespace BEZAOPayDAL
{
   public class BEZAODAL
   {
       private readonly string _connectionString;

       public BEZAODAL():
           this(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BEZAOPay;Integrated Security=True")
       {

       }
       
       public BEZAODAL(string connectionString )
       {
           _connectionString = connectionString;
       }


       private SqlConnection _sqlConnection = null;
       private void OpenConnection()
       {
           _sqlConnection = new SqlConnection { ConnectionString = _connectionString };
           _sqlConnection.Open();
       }

       private void CloseConnection()
       {
           if (_sqlConnection?.State != ConnectionState.Closed) 
               _sqlConnection?.Close();
       }

        public void InsertUser(string Name, string Email)
        {
            OpenConnection();
            string sql = $"Insert Into Users (Name, Email) values ('{Name}', '{Email}')";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }

        //public void InsertNewUser(User user)
        //{
        //    OpenConnection();
        //    string sql = "insert into users (name, email) values " + $"('{user.Name}', '{user.Email}'";

        //    using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
        //    {
        //        command.CommandType = CommandType.Text;
        //        command.ExecuteNonQuery();
        //    }
        //    CloseConnection();
        //}

        public void DeleteUser(int id)
        {
            OpenConnection();
            string sql = $"Delete from Users where Id = '{id}'";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                try
                {
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Exception error = new Exception("Sorry! You cannot delete this user!", ex);
                    throw error;
                }
                CloseConnection();
            }
        }

        public void UpdateUserName(int id, string newUserName)
        {
            OpenConnection();
            string sql = $"Update Users Set UserName = '{newUserName}' Where UserId = '{id}'";
            using (SqlCommand command = new SqlCommand(sql, _sqlConnection))
            {
                command.ExecuteNonQuery();
            }
            CloseConnection();
        }

       public IEnumerable<User> GetAllUsers()
       {
            OpenConnection();

            var users = new List<User>();

            var query = @"SELECT * FROM USERS";

            using (var command = new SqlCommand(query, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                   users.Add(new User
                   {
                       Id = (int) reader["Id"],
                       Name = (string) reader["Name"],
                       Email =  (string) reader["Email"]


                   }); 
                }
                reader.Close();
            }

            return users;

       }
    }
}
