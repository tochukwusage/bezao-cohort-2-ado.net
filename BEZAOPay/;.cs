﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using BEZAOPayDAL;
using BEZAOPayDAL.Models;
using static System.Console;


namespace BEZAOPay
{
    class Program
    {
        static void Main(string[] args)
        {
            //var db = new BEZAODAL();

            var connectionString = ConfigurationManager.ConnectionStrings["BEZAOConnect"].ConnectionString;
            var db = new BEZAODAL(connectionString);
            db.DeleteUser(15);
            db.InsertUser("Elochukwu", "Elochukwu@gmail.com");
            var users = db.GetAllUsers();

            foreach (var user in users)
            {
                Console.WriteLine($"Id: {user.Id}\nName: {user.Name}\nEmail: {user.Email}");
            }

        }
        private static int AddNewRecord()
        {
            using (var context = new BEZAOPayModel())
            {
                try
                {
                    var user = new User() { Name = "Sage", Email = "tochukwusage@domain.com" };
                    context.Users.Add(user);
                    context.SaveChanges();
                    return user.Id;
                }
                catch(Exception ex)
                {
                    WriteLine(ex.InnerException?.Message);
                    return 0;
                }
            }
        }
    }
}
