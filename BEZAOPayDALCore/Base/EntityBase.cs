﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAOPayDALCore.Base
{
    public class EntityBase
    {
        [Key]
        public int Id { get; set; }
        [Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
